﻿using System;

namespace CollectionsExtensions
{
    /// <summary>
    /// Defines a Java-like iterator interface.
    /// </summary>
    /// <remarks>
    /// Sometimes it's advantageous to have a HasCurrent property. It seems
    /// when writing parsers/lexers I'm always having to manually implement
    /// a "hasNext" or "hasCurrent" variable. This Iterator interface simplifies those usages.
    /// </remarks>
    /// <typeparam name="T"></typeparam>
    public interface IIterator<out T> : IDisposable
    {
        bool HasCurrent { get; }
        T Current { get; }
        void MoveNext();
    }
}