﻿using Prelude;
using String = Prelude.List<Prelude.Char>;

namespace CollectionsExtensions
{
    public class Program2
    {
        public static void Main()
        {
            IO<String> readLine = GetLine.Invoke;
            var _ = from s in readLine
                    select PutStrLn<String>.Invoke(s);

            List<Bool> list = List<Bool>.Cons(Bool.True)(List<Bool>.Nil);
            System.Console.WriteLine(list.Show);
        }
    }
}
