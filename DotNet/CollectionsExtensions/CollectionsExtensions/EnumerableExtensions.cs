﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionsExtensions
{

    public static class EnumerableExtensions
    {

        public static IEnumerable<Tuple<TA,TB>> Zip<TA,TB>(this IEnumerable<TA> xs, IEnumerable<TB> ys)
        {
            IEnumerator<TA> iterator1 = xs.GetEnumerator();
            IEnumerator<TB> iterator2 = ys.GetEnumerator();

            while(iterator1.MoveNext() && iterator2.MoveNext())
            {
                yield return new Tuple<TA, TB>(iterator1.Current, iterator2.Current);
            }
        }

        /// <summary>
        /// In linear time and space
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        public static IEnumerable<T> Append<T>(this IEnumerable<T> first, IEnumerable<T> second)
        {
            foreach (var element in first) yield return element;
            foreach (var element in second) yield return element;
        }

        public static bool And(this IEnumerable<bool> xs)
        {
            return xs.Aggregate((x,y) => x && y);
        }

        public static readonly Func<IEnumerable<bool>, bool> AndListFunc = And;

        public static IEnumerable<T> Cons<T>(this T head, IEnumerable<T> tail)
        {
            yield return head;
            foreach (var e in tail) yield return e;
        }

        public static Func<T, Func<IEnumerable<T>, IEnumerable<T>>> GetConsFunc<T>()
        {
            return ((Func<T,IEnumerable<T>,IEnumerable<T>>)Cons).ToCurry();
        }

        public static IEnumerable<TResult> Unfold<T,TResult>(Func<T,bool> pred, 
                                                             Func<T,TResult> head, Func<T,T> tail, T value)
        {
            return !pred(value)
                       ? head(value).Cons(Unfold(pred, head, tail, tail(value)))
                       : Enumerable.Empty<TResult>();
        }

        public static Func<Func<T,bool>,Func<Func<T,TResult>,Func<Func<T,T>,Func<T,IEnumerable<TResult>>>>> GetUnfoldFunc<T,TResult>()
        {
            return ((Func<Func<T,bool>,Func<T,TResult>,Func<T,T>,T,IEnumerable<TResult>>) Unfold).ToCurry();
        }

        public static IEnumerable<TOut> Map<TIn,TOut>(this Func<TIn,TOut> f, IEnumerable<TIn> xs)
        {
            var head = FuncHelper.GetComposeFunc<IEnumerable<TIn>, TIn, TOut>()(f)(GetHeadFunc<TIn>());
            return Unfold(GetEmptyFunc<TIn>(), head, GetTailFunc<TIn>(), xs);
        }

        public static Func<Func<TIn,TOut>,Func<IEnumerable<TIn>,IEnumerable<TOut>>> GetMapFunc<TIn,TOut>()
        {
            return ((Func<Func<TIn, TOut>, IEnumerable<TIn>, IEnumerable<TOut>>)Map).ToCurry();
        }

        public static Func<IEnumerable<T>,T> GetHeadFunc<T>()
        {
            return Enumerable.First;
        }

        public static Func<IEnumerable<T>, IEnumerable<T>> GetTailFunc<T>()
        {
            Func<IEnumerable<T>, int, IEnumerable<T>> skip = Enumerable.Skip;
            return skip.Flip().ToCurry()(1);
        }

        public static Func<IEnumerable<T>,bool> GetEmptyFunc<T>()
        {
            return FuncHelper.NotFunc.Compose<IEnumerable<T>,bool,bool>(Enumerable.Any);
        }

        public static IEnumerable<T> Iterate<T>(this T value, 
                                                Func<T,T> f)
        {
            return Unfold(FuncHelper.GetConstFunc<Boolean,T>()(false), FuncHelper.GetIdFunc<T>(), f, value);
        }

        public static Func<T,Func<Func<T,T>,IEnumerable<T>>> GetIterateFunc<T>()
        {
            return ((Func<T, Func<T, T>, IEnumerable<T>>)Iterate).ToCurry();
        }

        public static String Show<T>(this IEnumerable<T> xs)
        {
            var ys = Map(x => x.ToString(), xs);
            
            return "[" + String.Join(",", ys) + "]";
        }

        public static Func<IEnumerable<T>,String> GetShowFunc<T>()
        {
            return Show;
        }
    }

    public static class E<T>
    {
        public static readonly Func<T, Func<IEnumerable<T>, IEnumerable<T>>> Cons = EnumerableExtensions.GetConsFunc<T>();

        public static readonly Func<IEnumerable<T>, T> Head = EnumerableExtensions.GetHeadFunc<T>();

        public static readonly Func<IEnumerable<T>, IEnumerable<T>> Tail = EnumerableExtensions.GetTailFunc<T>();

        public static readonly Func<IEnumerable<T>, bool> Any = Enumerable.Any;

        public static readonly Func<IEnumerable<T>, bool> Empty = EnumerableExtensions.GetEmptyFunc<T>();

        public static readonly Func<T, Func<Func<T, T>, IEnumerable<T>>> Iterate = EnumerableExtensions.GetIterateFunc<T>();
    }
}