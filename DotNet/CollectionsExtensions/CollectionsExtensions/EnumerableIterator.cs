﻿using System.Collections.Generic;

namespace CollectionsExtensions
{
    /// <summary>
    /// Defines an implementation of <see cref="IIterator{T}"/> that
    /// works on any <see cref="IEnumerable{T}"/> instance.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EnumerableIterator<T> : IIterator<T>
    {
        private readonly IEnumerator<T> enumerator;
        private bool hasCurrent;

        public bool HasCurrent
        {
            get { return hasCurrent; }
        }

        public T Current
        {
            get { return enumerator.Current; }
        }

        public void MoveNext()
        {
            hasCurrent = enumerator.MoveNext();
        }

        private EnumerableIterator(IEnumerator<T> enumerator)
        {
            this.enumerator = enumerator;
            this.hasCurrent = enumerator.MoveNext();
        }

        /// <summary>
        /// Gets an iterator for the specified enumerable.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static IIterator<T> GetIterator(IEnumerable<T> enumerable)
        {
            return new EnumerableIterator<T>(enumerable.GetEnumerator());
        }

        public void Dispose()
        {
            enumerator.Dispose();
        }
    }
}