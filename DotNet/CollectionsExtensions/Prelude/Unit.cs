﻿using NotImplementedException = System.NotImplementedException;


namespace Prelude
{
    public struct Unit : IOrd<Unit>, IShow
    {
#pragma warning disable 649
        // I get a warning either way (If I assign it default value).
        public static readonly Unit Instance;
#pragma warning restore 649

        public F<Unit, Bool> EqualTo
        {
            get { return Const<Bool, Unit>.Invoke(Bool.True); }
        }

        public F<Unit, Bool> LessThan
        {
            get { return Const<Bool, Unit>.Invoke(Bool.False); }
        }

        public F<Unit, Bool> LessThanEqualTo
        {
            get { return Const<Bool, Unit>.Invoke(Bool.True); }
        }

        public F<Unit, Bool> GreaterThan
        {
            get { return Const<Bool, Unit>.Invoke(Bool.False); }
        }

        public F<Unit, Bool> GreaterThanEqualTo
        {
            get { return Const<Bool, Unit>.Invoke(Bool.True); }
        }

        public List<Char> Show
        {
            get { return S.Create("()"); }
        }
    }

    public class UnitReader : IRead<Unit>
    {
        public F<List<Char>, Unit> Read
        {
            get { throw new NotImplementedException(); }
        }
    }
}
