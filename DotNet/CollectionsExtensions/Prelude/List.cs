﻿using Type = System.Type;
using InvalidOperationException = System.InvalidOperationException;
namespace Prelude
{


    public abstract class List<T> : IOrd<List<T>>, IShow
    {


        public static readonly F<T, F<List<T>, List<T>>> Cons = hd => tl => new ConsType(hd, tl);
        public static readonly List<T> Nil = NilType.Instance;






        public abstract T Head { get; }
        public abstract List<T> Tail { get; }

        public abstract Bool Singleton { get; }
        public abstract Bool Doubleton { get; }

        public abstract Bool Null { get; }

        public abstract F<List<T>, Bool> EqualTo { get; }

        public abstract F<T1, Bool> GetElemFunc<T1>() where T1 : IEq<T1>;

        public abstract F<List<T>, Bool> LessThan { get; }
        protected abstract F<Comparison,F<List<T>, Bool>> Compare { get; }




        public abstract F<List<T>, Bool> LessThanEqualTo { get; }


        public abstract F<List<T>, Bool> GreaterThan { get; }


        public abstract F<List<T>, Bool> GreaterThanEqualTo { get; }


        private readonly Type typeofIShow = typeof(IShow);

        public List<Char> Show
        {
            get
            {
                if (!typeofIShow.IsAssignableFrom(typeof(T))) throw new System.NotSupportedException("not showable");
                var innerString = Join<Char>.Invoke(S.Create(","))(Map<T,List<Char>>.Invoke(t => ((IShow)t).Show)(this));
                return Append<Char>.Invoke(S.Create("["))(Append<Char>.Invoke(innerString)(S.Create("]")));
            }
        }

        protected enum Comparison
        {
            Lt,
            Lte,
            Gt,
            Gte,
            Eq
        }

        private sealed class ConsType : List<T>
        {
            private readonly T head;
            private readonly List<T> tail;
            public ConsType(T head, List<T> tail)
            {
                this.head = head;
                this.tail = tail;
            }

            public override T Head
            {
                get { return head; }
            }

            public override List<T> Tail
            {
                get { return tail; }
            }

            public override Bool Null
            {
                get { return false; }
            }

            public override Bool Singleton
            {
                get { return Tail == Nil; }
            }

            public override Bool Doubleton
            {
                get { return Tail != Nil && Tail.Singleton; }
            }

            private readonly Type typeofIeqT = typeof(IEq<T>);
            private readonly Type typeofIordT = typeof(IOrd<T>);
            private readonly Type typeofT = typeof(T);
            public override F<List<T>, Bool> EqualTo
            {
                get
                {
                    if (!typeofIeqT.IsAssignableFrom(typeofT))
                    {
                        throw new InvalidOperationException("EqualTo not supported because T doesn't support it");
                    }
                    return Compare(Comparison.Eq);
                }
            }

            public override F<T1, Bool> GetElemFunc<T1>()
            {
                return x =>
                           {
                               if (typeof(T1) != typeof(T)) return Bool.False;
                               T1 capture = (T1)(object)this;
                               return capture.EqualTo(x);
                           };
            }



            public override F<List<T>, Bool> LessThan
            {
                get
                {
                    AssertOrdered();
                    return Compare(Comparison.Lt);
                }
            }



            protected override F<Comparison,F<List<T>, Bool>> Compare
            {
                get
                {
                    return ord =>
                           xs =>
                               {
                                   if (xs.Null) return ord == Comparison.Gt || ord == Comparison.Gte;
                                   F<T, Bool> elementOrd = GetComparisonFunc(ord);
                                   return (elementOrd(xs.Head)) ? tail.Compare(ord)(xs.Tail) : Bool.False;
                               };
                }
            }

            private F<T, Bool> GetComparisonFunc(Comparison ord)
            {
                if (ord == Comparison.Eq)
                {
                    return ((IEq<T>)this.head).EqualTo;
                }

                var head1 = (IOrd<T>)this.head;
                F<T, Bool> elementOrd;
                switch(ord)
                {
                    case Comparison.Lt:
                        elementOrd = head1.LessThan;
                        break;
                    case Comparison.Lte:
                        elementOrd = head1.LessThanEqualTo;
                        break;
                    case Comparison.Gt:
                        elementOrd = head1.GreaterThan;
                        break;
                    case Comparison.Eq: // impossible case.
                        elementOrd = head1.EqualTo;
                        break;
                    default:
                        elementOrd = head1.GreaterThanEqualTo;
                        break;
                }
                return elementOrd;
            }

            private void AssertOrdered()
            {
                if (!typeofIordT.IsAssignableFrom(typeofT))
                {
                    throw new InvalidOperationException("EqualTo not supported because T doesn't support it");
                }
            }

            public override F<List<T>, Bool> LessThanEqualTo
            {
                get
                {
                    AssertOrdered();
                    return Compare(Comparison.Lte);
                }
            }

            public override F<List<T>, Bool> GreaterThan
            {
                get
                {
                    AssertOrdered();
                    return Compare(Comparison.Gt);
                }
            }

            public override F<List<T>, Bool> GreaterThanEqualTo
            {
                get
                {
                    AssertOrdered();
                    return Compare(Comparison.Gte);
                }
            }



        }

        private sealed class NilType : List<T>
        {
            public static readonly NilType Instance = new NilType();
            private NilType()
            {}

            public override T Head
            {
                get { throw new InvalidOperationException(); }
            }

            public override List<T> Tail
            {
                get { throw new InvalidOperationException(); }
            }

            public override Bool Singleton
            {
                get { return false; }
            }

            public override Bool Doubleton
            {
                get { return false; }
            }

            public override Bool Null
            {
                get { return Bool.True; }
            }

            public override F<List<T>, Bool> EqualTo
            {
                get { return xs => xs == this; }
            }

            public override F<T1, Bool> GetElemFunc<T1>() 
            {
                return x => Bool.False;
            }

            public override F<List<T>, Bool> LessThan
            {
                get { return xs => xs.Null.Not; }
            }

            public override F<List<T>, Bool> LessThanEqualTo
            {
                get { return xs => Bool.True; }
            }

            public override F<List<T>, Bool> GreaterThan
            {
                get { return xs => Bool.False; }
            }

            public override F<List<T>, Bool> GreaterThanEqualTo
            {
                get { return xs => xs.Null; }
            }


            protected override F<Comparison, F<List<T>, Bool>> Compare
            {
                get
                {
                    return ord =>
                               {
                                   switch (ord)
                                   {
                                       case Comparison.Lt:
                                           return LessThan;
                                       case Comparison.Lte:
                                           return LessThanEqualTo;
                                       case Comparison.Gt:
                                           return GreaterThan;
                                       case Comparison.Eq:
                                           return EqualTo;
                                       default:
                                           return GreaterThanEqualTo;
                                   }
                               };
                }
            }
        }


    }

    public static class L<T>
    {
        public static readonly F<List<T>, Bool> Null = xs => xs.Null;
    }
    public class ListReader<T> : IRead<List<T>>
    {
        public F<List<Char>, List<T>> Read
        {
            get { throw new System.NotImplementedException(); }
        }
    }

    public static class FoldLeft<TA,TB>
    {
        public static readonly F<F<TA, F<TB, TA>>, F<TA, F<List<TB>, TA>>> Invoke =
            f => s => xs => L<TB>.Null(xs) ? s : Invoke(f)(f(s)(xs.Head))(xs.Tail);
    }

    public static class Map<TA,TB>
    {
        public static readonly F<F<TA, TB>, F<List<TA>, List<TB>>> Invoke =
            f => xs => xs.Null ? List<TB>.Nil : List<TB>.Cons(f(xs.Head))(Invoke(f)(xs.Tail));
    }

    public static class Append<T>
    {
        public static readonly F<List<T>, F<List<T>, List<T>>> Invoke =
            xs => ys => xs.Null ? ys : List<T>.Cons(xs.Head)(Invoke(xs.Tail)(ys));
    }

    public static class Join<T>
    {

        public static readonly F<List<T>, F<List<List<T>>, List<T>>> Invoke =
            xs => yss => yss.Null
                             ? List<T>.Nil
                             : yss.Singleton
                                   ? yss.Head
                                   : yss.Doubleton
                                         ? Append<T>.Invoke(yss.Head)(Append<T>.Invoke(xs)(yss.Tail.Head))
                                         : Append<T>.Invoke(yss.Head)(Append<T>.Invoke(xs)(Invoke(xs)(yss.Tail)));
    }

}
