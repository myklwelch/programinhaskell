﻿namespace Prelude
{
    public interface INum<T> : IEq<T>, IShow
    {
        F<T, T> Add { get; }
        F<T, T> Subtract { get; }
        F<T, T> Multiply { get; }
        T Negate { get; }
        T Abs { get; }
        T Signum { get; }
    }

    public static class Num<T> where T : INum<T>
    {
        public static readonly F<T, F<T, T>> Add = x => y => x.Add(y);
        public static readonly F<T, F<T, T>> Subtract = x => y => x.Subtract(y);
        public static readonly F<T, F<T, T>> Multiply = x => y => x.Multiply(y);
        public static readonly F<T, T> Negate = x => x.Negate;
        public static readonly F<T, T> Abs = x => x.Abs;
        public static readonly F<T, T> Signum = x => x.Signum;
    }
}
