﻿namespace Prelude
{
    using String = List<Char>;

    public interface IRead<out T>
    {
        F<String, T> Read { get; }
    }
}
