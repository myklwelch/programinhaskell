﻿namespace Prelude
{
    using String = List<Char>;

    public struct Char : IOrd<Char>, IShow
    {
        private readonly char val;
        private Char (char val)
        {
            this.val = val;
        }
        public static implicit operator Char (char val)
        {
            return new Char (val);
        }

        public F<Char, Bool> EqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val == x.val;
            }
        }

        public F<Char, Bool> LessThan
        {
            get
            {
                var capture = this;
                return x => capture.val < x.val;
            }
        }

        public F<Char, Bool> LessThanEqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val <= x.val;
            }
        }

        public F<Char, Bool> GreaterThan
        {
            get
            {
                var capture = this;
                return x => capture.val > x.val;
            }
        }

        public F<Char, Bool> GreaterThanEqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val >= x.val;
            }
        }

        public String Show
        {
            get { return List<Char>.Cons(val)(List<Char>.Nil); }
        }

        public Int IntValue
        {
            get { return val; }
        }

        public static readonly F<Char, Bool> IsLower = c => System.Char.IsLower (c.val);
        public static readonly F<Char, Bool> IsUpper = c => System.Char.IsUpper (c.val);
        public static readonly F<Char, Bool> IsAlpha = c => System.Char.IsLetter (c.val);
        public static readonly F<Char, Bool> IsDigit = c => System.Char.IsDigit (c.val);
        public static readonly F<Char, Bool> IsAlphaNum = c => System.Char.IsLetterOrDigit (c.val);
        public static readonly F<Char, Bool> IsSpace = c => System.Char.IsWhiteSpace (c.val);
        public static readonly F<Char, Int> Ord = c => c.IntValue;
        public static readonly F<Int, Char> Chr = n => n.CharValue;

        public static readonly F<Char, Int> DigitToInt = c =>
                                                             {
                                                                 if (IsDigit(c))
                                                                     return Ord(c).Subtract(Ord('0'));
                                                                 throw new System.ArgumentException("Digit to Int only works on digits.");
                                                             };


        public static readonly F<Int, Char> IntToDigit = n =>
        {
            if (B.Or(n.GreaterThanEqualTo(0))(n.LessThanEqualTo(9))) return Chr(Ord('0').Add(n));
            throw new System.ArgumentException("IntToDigit requires a char in range [0..9]");
        };

        public static readonly F<Char, Char> ToLower = c => IsUpper(c) ? System.Char.ToLower(c.val) : c;
        public static readonly F<Char, Char> ToUpper = c => IsLower(c) ? System.Char.ToUpper(c.val) : c;
    }

    public class CharReader : IRead<Char>
    {
        public F<String, Char> Read
        {
            get
            {
                throw new System.NotImplementedException ();
            }
        }
    }
    
    
}
