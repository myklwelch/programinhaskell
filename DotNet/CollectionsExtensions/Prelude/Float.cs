﻿namespace Prelude
{
    using String = List<Char>;

    public struct Float : IFractional<Float>
    {
        private readonly double val;
        private Float(double val)
        {
            this.val = val;
        }
        public static implicit operator Float(double val)
        {
            return new Float(val);
        }
        public static readonly Float One = 1;

        public F<Float, Bool> EqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val == x.val; 
            }
        }

        public F<Float, Bool> LessThan
        {
            get
            {
                var capture = this;
                return x => capture.val < x.val;
            }
        }

        public F<Float, Bool> LessThanEqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val <= x.val;
            }
        }

        public F<Float, Bool> GreaterThan
        {
            get
            {
                var capture = this;
                return x => capture.val > x.val;
            }
        }

        public F<Float, Bool> GreaterThanEqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val >= x.val;
            }
        }

        public String Show
        {
            get { return S.Create(val.ToString()); }
        }

        public F<Float, Float> Add
        {
            get
            {
                var capture = this;
                return x => capture.val + x.val;
            }
        }

        public F<Float, Float> Subtract
        {
            get
            {
                var capture = this;
                return x => capture.val - x.val;
            }
        }

        public F<Float, Float> Multiply
        {
            get
            {
                var capture = this;
                return x => capture.val * x.val;
            }
        }

        public Float Negate
        {
            get { return -val; }
        }

        public Float Abs
        {
            get { return val < 0 ? -val : val; }
        }

        public Float Signum
        {
            get { return val < 0 ? -1 : 1; }
        }

        public F<Float, Float> Div
        {
            get 
            { 
                var capture = this;
                return x => capture.val / x.val;
            }
        }

        public Float Recip { get { return new Float(1.0 / val); } }
    }

    public class FloatReader : IRead<Float>
    {
        public F<String, Float> Read
        {
            get { throw new System.NotImplementedException(); }
        }
    }
}
