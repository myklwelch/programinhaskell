﻿namespace Prelude
{
    public delegate T Lazy<out T>(Unit _);

    public class L<T>
    {
        private readonly Lazy<T> lazy;
        private readonly WriteOnce<T> evaluatedValue = new WriteOnce<T>();
        private L(Lazy<T> lazy)
        {
            this.lazy = lazy;
        }
        public static implicit operator L<T>(Lazy<T> lazy)
        {
            return new L<T>(lazy);
        }
        

        public T Value
        {
            get
            {
                return (evaluatedValue.HasValue)
                           ? evaluatedValue.Value
                           : evaluatedValue.Value = lazy(Unit.Instance);
            }
        }



    }
}
