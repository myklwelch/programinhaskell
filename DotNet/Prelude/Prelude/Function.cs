﻿namespace Prelude
{
    public delegate TOut F<in TIn, out TOut>(Lazy<TIn> arg);

}
