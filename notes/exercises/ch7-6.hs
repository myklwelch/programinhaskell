import Prelude hiding (curry,uncurry)

-- Take a function on pairs and return a curried function
curry :: ((a,b) -> c) -> (a -> b -> c)
curry f = \x -> \y -> f (x,y)

-- Take a curried function and return a function on pairs
uncurry :: (a -> b -> c) -> ((a,b) -> c)
uncurry f = \(x,y) -> f x y
