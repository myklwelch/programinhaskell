-- The sum of the first 100 squares
expr = sum [x^2|x<-[1..100]]
