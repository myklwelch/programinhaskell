{-# LANGUAGE CPP #-}
#include "ch6-4.hs"

halve :: [a] -> ([a],[a])
halve xs = splitAt ((length xs) `div` 2) xs

msort :: Ord a => [a] -> [a]
msort [] = []
msort l@(x:[]) = l
msort xs = merge firstList secondList
    where firstList = msort $ fst p
          secondList = msort $ snd p
	  p = halve xs

-- A simple optimization (I think). 
-- Handle doubleton lists specially.
msort2 :: Ord a => [a] -> [a]
msort2 [] = []
msort2 l@(x:[]) = l
msort2 l@(x:y:[]) | x < y     = l
                  | otherwise = y:x:[]
msort2 xs = merge firstList secondList
    where firstList = msort2 $ fst p
          secondList = msort2 $ snd p
	  p = halve xs
