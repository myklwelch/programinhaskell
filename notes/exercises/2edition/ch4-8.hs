luhnDouble :: Int -> Int
luhnDouble x | x < 5 = x * 2
             | otherwise = (x * 2) - 9

luhn :: Int -> Int -> Int -> Int -> Bool
luhn w x y z = (luhnDouble w + x + luhnDouble y + z) `mod` 10 == 0
