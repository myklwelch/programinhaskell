module Chap9 where

import Prelude hiding (getLine)
import System.IO hiding (getLine)

getCh :: IO Char
getCh = hSetEcho stdin False >>= \_ ->
        getChar >>= \c ->
        hSetEcho stdin True >>= \_ ->
        return c

-- getChar :: IO Char
-- getChar = getCh >>= \c ->
          -- putChar c >>= \_ ->
          -- return c

getLine :: IO String
getLine = getChar >>= \c ->
          if (c=='\n') then return ""
          else getLine >>= \cs ->
               return (c:cs)
        
type Pos = (Int,Int)

seqn :: [IO a] -> IO ()
seqn [] = return ()
seqn (a:as) = a >>= \_ ->
              seqn as 

goto :: Pos -> IO ()
goto (x,y) = putStr ("\ESC[" ++ show y ++ ";" ++ show x ++ "H")

writeat :: Pos -> String -> IO ()
writeat p xs = goto p >>= \_ ->
               putStr xs

beep :: IO ()
beep = putStr "\BEL"

cls :: IO ()
cls = putStr "\ESC[2J"
