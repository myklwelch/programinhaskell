import Prelude hiding (sum,take,last)

sum :: Num a => [a] -> a
sum [] = 0
sum (x:xs) = x + sum xs

-- simplified
sum2 :: Num a => [a] -> a
sum2 = foldr (+) 0 


take :: Int -> [a] -> [a]
take 0 _ = []
take _ [] = []
take n (x:xs) | n < 0     = []
              | otherwise = x : take (n-1) xs

-- simplified?
take2 :: Int -> [a] -> [a]
take2 n xs = map snd (zip [1..n] xs)

take3 :: Int -> [a] -> [a]
take3 n _ | n <= 0 = []
take3 _ [] = []
take3 n (x:xs) = x : take3 (n-1) xs

-- As you'll see, I wrote many versions of last.
-- I think this is probably the best. 
last :: [a] -> a
last [x] = x
last (x:xs) = last xs

-- simplified?
-- Nah, this goes thru the list twice
last2 xs = xs !! ((length xs) - 1)

-- this is allocating a lot more memory than last
last3 xs = head $ reverse xs

-- only works for numeric types, and for lists whose
-- members are all >= 0. (If we had some default function we
-- expand this to any type that has a default function and
-- lists that are made up of non-default values)
-- Algorithm: Returns an element from the list only if the
--            current aggregate is < 0 which only happens 
--            on the last element.
--
-- The craziness comes from having to make sure that we 
-- don't return -1 for empty list. That's what the 
-- checkForNeg is doing.
last4 :: (Num a,Ord a) => [a] -> a
last4 = checkForNeg . foldr firstPositive (-1)
    where checkForNeg = (\r -> 
              if r < 0 then error "empty list" else r) 
          firstPositive = 
              (\a -> \b -> if b < 0 then a else b) 

-- A variant on last. Requires a default value for
-- empty lists. (Well really not because of empty lists, but
-- because foldl requires a default value for the 
-- "aggregator". And there isn't a "defaultValue" function.
last5 :: a -> [a] -> a
last5 dv = foldl (\x -> \y -> y) dv 

-- A variation on last5. But I didn't want to use
-- a lambda. I wanted to use a built in function. It
-- seems like a simple function. Well I found one 
-- called snd which is used for pairs. It always returns
-- the second value of a pair. This is what I want, but in
-- a curried form.
last6 :: a -> [a] -> a
last6 dv = foldl (curry snd) dv

-- A function that always returns its second parameter
second :: a -> b -> b
second x y = y

-- I quite like this version. And it handles empty lists
-- nicely as well.
last7 :: a -> [a] -> a
last7 dv = foldl second dv

-- Okay, to get rid of the default value problem we need
-- a new version of foldl that always requires a list with
-- at least one element. Then we can use the head of the 
-- list as the default value and defer to foldl. LINQ has 
-- such a method called aggregate.
aggregate :: (a -> a -> a) -> [a] -> a
aggregate _ [] = error "empty list"
aggregate _ [x] = x
aggregate f (x1:xs) = foldl f x1 xs

-- GOLD STANDARD! :-) (At least I would consider it so
-- if aggregate and second where considered "common" 
-- functions.) But seriously, even though it is concise
-- I think it's doing more work than last and last2
last8 :: [a] -> a
last8 = aggregate second

-- Note: this is cheating and bad form. 
-- The function seq does the "right" thing but
-- it is used to force strict evaluation on it's first 
-- argument. So this version would cause me to evaluate 
-- every element in my list just to get to the last 
-- element. This may be fine for lists of already evaluated 
-- values. But in general your list probably doesn't have 
-- evaluated values already.
last9 :: [a] -> a
last9 = aggregate seq
