-- rewrite the following expression using
-- two comprehensions with one generator each
expr1 = [(x,y) | x <- [1,2,3], y <- [4,5,6]]

-- Note: I had to cheat and google for the 
-- answer. It wasn't clear to me what nested list
-- comprehensions meant. I was attempting to nest
-- inside the compartment after the '|' instead of 
-- before it. Once you see that trick its easy.
expr2 = concat [[(x,y) | y <- [4,5,6]] | x <- [1,2,3]]


