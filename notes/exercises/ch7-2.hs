import Prelude hiding (all,any,takeWhile,dropWhile)
all :: (a -> Bool) -> [a] -> Bool
all p = foldr (\x -> (&& (p x))) True

all2 :: (a -> Bool) -> [a] -> Bool
all2 p = foldr ((&&) . p) True

any :: (a -> Bool) -> [a] -> Bool
any p = foldr (\x -> (|| (p x))) False

any2 :: (a -> Bool) -> [a] -> Bool
any2 p = foldr ((||) . p) False

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ [] = []
takeWhile p (x:xs) | p x = x : takeWhile p xs
                   | otherwise = []

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile _ [] = []
dropWhile p l@(x:xs) | p x = dropWhile p xs
                     | otherwise = l
