-- write [ f x : x <- xs, p x ] using map and filter

select_where :: (a -> b) -> (a -> Bool) -> [a] -> [b]
select_where f p = map f . filter p
