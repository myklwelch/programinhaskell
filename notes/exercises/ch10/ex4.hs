data Tree = Leaf Int | Node Tree Tree deriving(Show)

halve :: [Int] -> ([Int],[Int])
halve l = splitAt ((length l) `div` 2) l

balance :: [Int] -> Tree
balance [] = error "no elements"
balance (x:[]) = Leaf x
balance xs = Node (balance l) (balance r)
    where (l,r) = halve xs
