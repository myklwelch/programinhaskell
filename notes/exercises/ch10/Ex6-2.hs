module Main where
import Ex5
import Ex6
import Assoc
import MyParser
import Prelude hiding ((>>=),return)

main = do
           putStrLn "Enter a proposition"
           xs <- getLine
           check xs

check :: String -> IO ()
check xs = if (isTautology xs)
           then
               putStrLn "Is tautology"
           else
               putStrLn "Is not a tautology"

isTautology :: String -> Bool
isTautology xs = isTaut p
    where [(p,[])] = parse expr xs
