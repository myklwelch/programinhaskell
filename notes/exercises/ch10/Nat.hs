module Nat where

data Nat = Zero | Succ Nat deriving (Eq, Ord)

add :: Nat -> Nat -> Nat
add Zero n = n
add (Succ m) n = Succ (add m n)

nat2int :: Nat -> Int
nat2int Zero = 0
nat2int (Succ m) = 1 + (nat2int m)

int2nat :: Int -> Nat
int2nat 0 = Zero
int2nat m = Succ (int2nat (m-1))

instance Show Nat where
    show = show . nat2int
