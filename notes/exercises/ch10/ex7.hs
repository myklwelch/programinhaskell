type ControlStack = [Op]

data Expr = Val Int | Add Expr Expr | Mult Expr Expr

data Op = EVALA Expr | ADD Int | MULT Int | EVALM Expr

eval :: Expr -> ControlStack -> Int
eval (Val n) c = exec c n
eval (Add n m) c = eval n (EVALA m:c)
eval (Mult n m) c = eval n (EVALM m:c)

exec :: ControlStack -> Int -> Int
exec [] n = n
exec ((EVALA e):c) n = eval e ((ADD n):c)
exec ((EVALM e):c) n = eval e ((MULT n):c)
exec ((ADD m):c) n = exec c (n+m)
exec ((MULT m):c) n = exec c (n*m)

value :: Expr -> Int
value e = eval e []
