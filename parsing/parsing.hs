-- This file is pretty much a copy of the parsing example in the text.
-- It is slightly different, since I tried to write most of it from scratch
-- from memory. One major difference is declaring Parser as a new data
-- type so that it can be made an instance of Monad.

module Parsing
  (
    Parser,
    parse,
    success,
    failure,
    char,
    sat,
    letter,
    digit,
    upper,
    lower,
    alphanum,
    string,
    (+++),
    many,
    many1,
    space,
    ident,
    nat,
    token,
    identifier,
    natural,
    symbol,
    (++++)
  ) where 
import Data.Char
infixr 5 +++

data Parser a =  P (String -> Maybe (a, String)) 

parse :: Parser a -> String -> Maybe (a, String)
parse (P p) s = p s

parse' :: Parser a -> String -> a
parse' p s = result
             where Just (result, _) = parse p s

success :: a -> Parser a
success v = return v

failure :: Parser a
failure = P (\inp -> Nothing)

item :: Parser Char
item = P (\inp -> case inp of
                            [] -> Nothing
                            (x:xs) -> Just (x, xs))

char :: Char -> Parser Char
char c = P (\inp -> case inp of
                      (x:xs) | c == x -> Just (x, xs)
                      _ -> Nothing)

sat :: (Char -> Bool) -> Parser Char
sat pred = do
               c <- item
               if (pred c) then return c else failure

letter :: Parser Char
letter = sat isLetter

digit :: Parser Char
digit = sat isDigit

upper :: Parser Char
upper = sat isUpper

lower :: Parser Char
lower = sat isLower

alphanum :: Parser Char
alphanum = sat isAlphaNum

string :: String -> Parser String
string [] = return ""
string (c:cs) = do
                   char c
                   string cs
                   return (c:cs)

(+++) :: Parser a -> Parser a -> Parser a
f +++ g = P (\input -> let parseResult = (parse f input) in 
                         case parseResult of
                             Nothing -> parse g input
                             Just(v, out) -> parseResult)


many :: Parser a -> Parser [a]
many p = (many1 p) +++ (return [])

many1 :: Parser a -> Parser [a]
many1 p = do
              v <- p
              vs <- many p
              return (v:vs)

space :: Parser ()
space = do
                 cs <- many (sat isSpace)
                 return ()

ident :: Parser String
ident = do
            c <- lower
            cs <- many alphanum
            return (c:cs)

nat :: Parser Int
nat = do
            d <- digit
            ds <- many digit
            return (read (d:ds) :: Int)

token :: Parser a -> Parser a
token p = do space
             v <- p
             space
             return v

identifier :: Parser String
identifier = token ident

natural :: Parser Int
natural = token nat

symbol :: String -> Parser String
symbol xs = token $ string xs


instance Monad Parser where
    p >>= f = P (\input -> case (parse p input) of
                                         Nothing -> Nothing
                                         Just (v, out) -> parse (f v) out)
    return a = P (\input -> Just (a, input))

instance Functor Parser where
    fmap f p = do
                   v <- p
                   return (f v)

-- Concatenation parser operator.
(++++) :: Parser [a] -> Parser [a] -> Parser [a]
f ++++ g = do
               xs <- f
               ys <- g
               return (xs ++ ys)
