> module Regex(accepts,Pattern) where 
> import Control.Monad
> import Parsing

> subseqs :: String -> [String]
> subseqs [] = []
> subseqs s@(x:xs) = s : subseqs xs

  A Pattern is just an alias for a string. That way it makes it
  easier to tell from the type of "accepts" which argument is which.

> type Pattern = String

  Checks to see if the specified pattern accepts the specified
  string. 

> accepts :: Pattern -> String -> Bool
> accepts p s = let parser = compile p
>                   result = parse parser s in
>               case result of
>                  Nothing -> False
>                  Just(_,[]) -> True
>                  Just(_,(x:_)) -> False
           
  Compiles a regular expression pattern into a parser that parses
  strings that match the pattern.

> compile :: Pattern -> Parser String
> compile [] = string ""
> compile s@(x:_) = case parse orexpr s of
>                       Nothing -> failure
>                       Just (result, []) -> result
>                       Just (result, (x:_)) -> failure



> orexpr :: Parser (Parser String)
> orexpr = let option1 = do
>                           re <- concatexpr
>                           char '|'
>                           oe <- orexpr
>                           return (re +++ oe)
>              option2 = concatexpr
>          in option1 +++ option2

> concatexpr :: Parser (Parser String)
> concatexpr = let option1 = do
>                               oe <- repeatexpr
>                               ce <- concatexpr
>                               return (liftM2 (++) oe ce)
>                  option2 = repeatexpr
>              in option1 +++ option2

> repeatexpr :: Parser (Parser String)
> repeatexpr = let option1 = do
>                               be <- basicexpr
>                               char '*'
>                               return (liftM concat (many be))
>                  option2 = basicexpr
>              in option1 +++ option2
                

> basicexpr :: Parser (Parser String)
> basicexpr = parenexpr +++ charexpr

> parenexpr :: Parser (Parser String)
> parenexpr = do
>                char('(')
>                p <- orexpr
>                char(')')
>                return p

> charexpr :: Parser (Parser String)
> charexpr = do
>               c <- regchar
>               return (string $ c:[])

> regchar :: Parser Char
> regchar = sat (\c -> c /= '|' && c /= '*' && c /= '(' && c /= ')')
