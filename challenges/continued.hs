module ContinuedFractions(CFract(), toCFract, toCFractN, fromCFract, parseCFract,
   parseList, parseMoreNums ) where
import Data.String.Utils (replace)
import MyParser 
import Prelude hiding (return, (>>=))

data CFract = MyFract [Integer] 

-- Converts a double to aCFract, stopping when the error is less 
-- than 1/1000000 or we have generated 10 integers.
toCFract :: Double -> CFract
toCFract = MyFract . (flip toCFractRound 10)

fromCFract :: CFract -> Double
fromCFract (MyFract l) = fromCFractI (map fromInteger l)

fromCFractI :: [Double] -> Double
fromCFractI = foldr1 addPartial 
    where addPartial next current = next + (1.0 / current)

-- Converts a double to a CFract. It stops generating
-- integers when we get an exact answer or we have generated
-- 10 integers.
toCFractN :: Double -> Int -> CFract
toCFractN d n = MyFract $ toCFractI d n
		  
		  
toCFractI :: Double -> Int -> [Integer]
toCFractI 0.0 _ = [0]
toCFractI _ 0 = []
toCFractI d n
    | fracPart == 0.0 || rest == [0] = [i]
    | otherwise                      = i : rest
    where i = floor d
          fracPart = d - (fromInteger i)
          rest = toCFractI (1.0 / fracPart) (n - 1)
		  
		  
toCFractRound :: Double -> Int -> [Integer]
toCFractRound 0.0 _ = [0]
toCFractRound _ 0 = []
toCFractRound d n
    | fracPart == 0.0 || recip > 1000000.0 || rest == [0] = [i]
    | otherwise                                           = i : rest
    where i = floor d
          fracPart = d - (fromInteger i)
          recip = 1.0 / fracPart
          rest = toCFractRound recip (n - 1)


instance Show CFract where
    show = showCFract
	
showCFract :: CFract -> String
showCFract c = "[" ++ (showCFractI c) ++ "]"
showCFractI (MyFract (x:[])) = show x
showCFractI (MyFract (x:y:ys)) = prefix ++ suffix
    where prefix = (show x) ++ ";" ++ (show y)
          suffix = foldr addElem "" ys
          addElem elem lst = "," ++ (show elem) ++ lst
		  
instance Read CFract where
    readsPrec _ s = readsCFract s
	
readsCFract :: ReadS CFract
readsCFract s = [(MyFract l,r) | (l,r) <- parse parseCFract s]

parseCFract :: Parser [Integer]
parseCFract = symbol "[" >>= \_ ->
              parseList >>= \ns ->
              symbol "]" >>= \_ ->
              return ns
          
parseList :: Parser [Integer]
parseList = (int >>= \n ->
             symbol ";" >>= \_ ->
             natural >>= \m ->
             parseMoreNums >>= \ns ->
             return ((toInteger n):(toInteger m):ns))
            +++ (natural >>= \n -> return [toInteger n])
           

parseMoreNums :: Parser [Integer]
parseMoreNums = many (symbol "," >>= \_ ->
                      natural >>= \n ->
                      return (toInteger n))

int :: Parser Int
int = (symbol "-" >>= \_ ->
       natural >>= \n ->
       return (-n)) +++ natural

-- CFract ::= [ list ]
-- list ::= n | n ; m morenums
-- morenums ::= , m morenums

