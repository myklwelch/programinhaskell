{-# LANGUAGE CPP #-}
#include "regex.hs"

digit = "(0|1|2|3|4|5|6|7|8|9)"
areacode = digit ++ digit ++ digit
exchange = digit ++ digit ++ digit
lastfour = digit ++ digit ++ digit ++ digit

localNum = "(" ++ exchange ++ "(.|-)" ++ lastfour ++ ")"
phone1 = "(" ++ areacode ++ "." ++ exchange ++ 
    "." ++ lastfour ++ ")"
phone2 = "(" ++ areacode ++ "-" ++ exchange ++ 
    "-" ++ lastfour ++ ")"

phoneNum = localNum ++ "|" ++ phone1 ++ "|" ++ phone2
isPhoneNum = accepts phoneNum

cfractSingle = "[" ++ digit ++ "]"
cfractDouble = "[" ++ digit ++ ";" ++ digit ++ "]"
cfractMany = "[" ++ digit ++ ";" ++ digit ++ "(" ++ 
             "," ++ digit ++ ")*" ++ "]"

isCFract = accepts ("(" ++ cfractSingle ++ ")|("
           ++ cfractDouble ++ ")|(" 
	   ++ cfractMany ++ ")")


