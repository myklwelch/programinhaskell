\documentclass[10pt]{amsart}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{url}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{courier}
\usepackage[usenames,dvipsnames]{color}
\usepackage{textcomp}
\usepackage{hyperref}
\usepackage{xspace}
\usepackage{enumerate}
\begin{document}

\newcommand{\navyblue}{\color{blue}}
\newcommand{\defined}{\mathrel{\text{::=}}}
\renewcommand{\lor}{\mathbin{\text{\textbf{or}}}}
\lstset{language=Haskell}
\lstset{upquote=true}
\lstset{numberstyle=\tiny,basicstyle=\small\ttfamily,keywordstyle=\color{blue},extendedchars=true}
\newcommand{\inlcsharp}{\lstinline[breaklines=true,language={[Sharp]C},keywordstyle=\ttfamily]}
\newcommand{\inlhaskell}{\lstinline[breaklines=true,language=Haskell,keywordstyle=\ttfamily]}
\newcommand{\inlh}{\lstinline[breaklines=true,language=Haskell,keywordstyle=\ttfamily]}
\newcommand{\csharp}{C\#\xspace}
\newcommand{\problem}{\textbf{Problem:}\xspace}
\newcommand{\solution}{\textbf{Solution:}\xspace}
\title{Regular Expressions Library}
%\author{Michael Welch}
\maketitle

\noindent This assignment is designed to replace most of the Chapter 8
exercises. It focuses more on programming with parsers and less on
parse trees and parsing theory.

Regular expressions are character patterns that describe a set of strings.
In this exercise we will write a library that allows us to test whether
a given string is ``accepted'' by a regular expression. (Alternatively,
it can be said that we will be testing whether a regular expression
\emph{matches} a string.)

\section{Grammar of Regex}
\label{sec:grammar}
Describing this grammar gets confusing, because we use parentheses and `|'
both in describing the grammar and in the grammar. So rather than use `|'
like the book does for choices I'm going to use $\lor$.
And rather than use parentheses for grouping I'm going to use curly 
braces. So, for example, if you look at figure~\ref{fig:fullgrammar} you 
will see $orexpr$ is defined to
be a $concatexpr$ followed by a choice. The first choice is a `|' followed by
an $orexpr$. The second choice is the empty string (denoted by the symbol
$\epsilon$).

\begin{figure}
\caption{The grammar for regular expressions.}
\label{fig:fullgrammar}
\begin{align*}
regexpr & \defined orexpr \\
orexpr  & \defined concatexpr \{\, \mathop{|} orexpr \lor \epsilon \} \\
concatexpr & \defined repeatexpr \{ concatexpr \lor \epsilon \} \\
repeatexpr & \defined basicexpr \{ * \lor \epsilon \} \\
basicexpr  & \defined parenexpr \lor charexpr \\
parenexpr  & \defined ( regexpr ) \\
charexpr   & \defined regchar \\
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}


A regular expression is built up of characters and operators. The most basic
regular expressions only use 3 operations: concatenation, alternation (option),
and repetition. Concatenation is denoted by just writing two regular
expressions side by side (as in ab). Alternation is denoted by the "|" symbol
and means either the first regex or the second (as in a|b).  Repetition is
denoted by "*" and means 0 or more of the preceding regular expression (as in
a*).

See table~\ref{tab:regex} for some examples of regular expressions:

\begin{table}
\caption{Examples of regular expressions and strings that they match.}
\label{tab:regex}
\begin{tabular}{lll}
   Pattern & Strings & Notes\\ \hline
   a                  & \{ a \} & Matches only "a" \\
   b                  & \{ b \} & Matches only "b" \\
   ab                 & \{ ab \}& Matches only "ab"\\
   a | b              & \{ a, b \} & Matches "a" or "b"\\
   a*                 & \{ $\epsilon$, a, aa, aaa, $\ldots$ \} & Matches an 
                        infinite set \\
   a(bc*)*d           & \{ ad, abd, abbd, abcd, abbcd, $\ldots$ \} & Matches
                        an infinite set
\end{tabular}
\end{table}

\section{Exercises}
If you wish, you can just read the grammar in section~\ref{sec:grammar} 
and just start implementing it any way you see fit. You can
always browse the exercises for hints and tips on how to proceed.

Otherwise you can work your way thru the exercises below. They build on one
another. Do as many as you care to. If you do them all you will have a basic
implementation of regular expressions.

In the exercises we 
will be implementing one part of the grammar at a time. Each exercise
will show you the partial grammar it is implementing, and 
\textcolor{blue}{highlight} 
any sections that are modified from the previous exercise.

\begin{enumerate}[1.]
\item \label{ex:regchar} Write a function \inlh=regchar :: Parser Char= that
parses any character except for the following four: `|', `*', `(', and `)'.
(These are special characters in regular expressions and therefore we won't
treat them as characters.  This limits our language but that's okay for our
purposes.) See figure~\ref{fig:regchar} for the grammar.
\begin{lstlisting}
> parse regchar "ab"
[('a', "b")]
> parse regchar "(a"
[]
\end{lstlisting}

\begin{figure}
\caption{The grammar for $regchar$ in exercise~\ref{ex:regchar}}
\label{fig:regchar}
\begin{align*}
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}

\item \label{ex:charexpr} Write a function 
\inlh=charexpr :: Parser(Parser String)=. This function parses a
single character pattern
and return a parser that accepts just that character.
See figure~\ref{fig:charexpr} for the new grammar.

\begin{lstlisting}[numbers=left]
> let [(p,_)] = parse charexpr "a"
> parse p "a"
[("a","")]
> parse p "b"
[]
\end{lstlisting}

\begin{figure}
\caption{The grammar for $charexpr$ in exercise~\ref{ex:charexpr}}
\label{fig:charexpr}
\begin{align*}
\navyblue charexpr & \navyblue \defined regchar \tag{new} \\
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}

Let me explain this example. 
The function \inlh=charexpr= is parsing a regular expression
pattern. It then returns a parser that parses whatever the pattern described.
So on line 1 we are parsing the regular expression ``a''. This is a simple 
regular expression that only ever matches ``a''. 

Parsers always return a list of tuples. We expect on line 1 that
ours will succeed and have one tuple. The first thing in
the tuple is the thing we parsed. In this case p is a \inlh=Parser String= that
only parses the letter a. So if we apply it (as we do on line 2) to ``a'' it
succeeds. If we apply it to ``b'' (line 4) it fails.

\textbf{N.B.}\footnote{Nota bene, or ``note well''. My adviser always used that
abbreviation and for years I had no idea what it meant.  I finally googled it
a few months ago and now I can use it with you. In addition, it gave
me a good excuse to try out the footnote command in \LaTeX\ as well. (I
also got to typeset \LaTeX. Is anybody reading this?)}  Don't be deceived by
the grammar. The function, \inlh=charexpr=, does have to do some work besides
just calling \inlh=regchar=. See that the two have different types.

\item \label{ex:regexpr} This is an easy one. It will be convenient to define a
top-level production called $regexpr$ which we'll update in every exercise
as we add more to our library. Write a function 
\inlh=regexpr :: Parser (Parser String)= that just defers to charexpr 
as shown in figure~\ref{fig:regexpr}.

\begin{lstlisting}[numbers=left]
> let [(p,_)] = parse regexpr "a"
> parse p "a"
[("a","")]
> parse p "b"
[]
\end{lstlisting}

\begin{figure}
\caption{The grammar for $regexpr$ in exercise~\ref{ex:regexpr}}
\label{fig:regexpr}
\begin{align*}
\navyblue regexpr & \navyblue \defined  charexpr  \tag{new} \\
charexpr   & \defined regchar \\
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}


\item \label{ex:compile} In this exercise we are going to write
a function named \inlh=compile= that will make it easier to test.
Notice in exercise~\ref{ex:charexpr} that it was clumsy to get at the parser.
This is because it was returned in a tuple inside of a list. The 
\inlh=compile= function will have the type \inlh=String -> Parser String=.
It will do the job of retrieving the parser out of the tuple inside
of the list so we can write the following:

\begin{lstlisting}[numbers=left]
> let p = compile "a"
> parse p "a"
[("a","")]
> parse p "b"
[]
\end{lstlisting}

Note: the compile function should return the \inlh=failure= parser if the 
entire regular expression is not consumed. For example, since we currently
only support single char regular expressions the regular expression ``ab''
cannot be parsed and returns the \inlh=failure= parser:

\begin{lstlisting}[numbers=left]
> let p = compile "ab"
> parse p "a"  -- this will fail to parse
[]
\end{lstlisting}

\item \label{ex:basicexpr} Write the function 
\inlh=parenexpr :: Parser (Parser String)= which is able to parse a
parenthesized regular expression like ``(b)''. And then write the function
\inlh=basicexpr :: Parser (Parser String)= which is able to parse either a
$charexpr$ or a $parenexpr$. Finally update
\inlh=regexpr= to call \inlh=basicexpr= instead of \inlh=charexpr=. When you
are done you should be able to compile regular expressions that are a single
character or a single character in parentheses. (Note: the parentheses aren't
matched. They are used to control grouping just like in arithmetic operations.
So the following two regular expressions are equivalent: "(c)" and "c".) See
figure~\ref{fig:basicexpr} for the grammar.

\begin{lstlisting}[numbers=left]
*Main> let p = compile "(c)"
*Main> parse p "cd"
[("c","d")]
*Main> let p = compile "c"
*Main> parse p "cd"
[("c","d")]
\end{lstlisting}

\begin{figure}
\caption{The grammar for $basicexpr$ and $parenexpr$ for 
exercise~\ref{ex:basicexpr}.}
\label{fig:basicexpr}
\begin{align*}
regexpr & \defined  \navyblue basicexpr  \tag{changed} \\
\navyblue basicexpr  & \navyblue \defined parenexpr \lor charexpr \tag{new}\\
\navyblue parenexpr  & \navyblue \defined ( regexpr ) \tag{new}\\
charexpr   & \defined regchar \\
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}

\item \label{ex:repeatexpr} Now we are ready for our first operator, the `*'
character. This presence of the operator means the previous
regular expression repeats 0 or more times. Write the function 
\inlh=repeatexpr :: Parser (Parser String)= that parses a $basicexpr$ followed
by a * or the empty string $\epsilon$. (See figure~\ref{fig:repeatexpr}.)  If
it parses a * then it should return a parser that matches the pattern described
by the $basicexpr$ many times. Else it should return a parser that only matches
the pattern once. Update \inlh=regexpr= to call \inlh=repeatexpr= instead
of \inlh=basicexpr=. 

\begin{lstlisting}[numbers=left,commentstyle=\color{OliveGreen}]
> let p = compile "a"
> parse p "aa" -- consumes only one a
[("a","a")]
> let q = compile "a*"
> parse q "aa" -- consumes all the a's
[("aa", "")]
> let r = compile "(a)*"
> parse r "aaaa" -- consumes all the a's
[("aaaa","")]
> parse r "bbbcd" -- consumes 0 a's (not a failure)
[("","bbbcd")]
\end{lstlisting}

\begin{figure}
\caption{The grammar for $repeatexpr$ for exercise~\ref{ex:repeatexpr}.}
\label{fig:repeatexpr}
\begin{align*}
regexpr & \defined  \navyblue repeatexpr  \tag{changed} \\
\navyblue repeatexpr & \navyblue \defined basicexpr \{* \lor \epsilon\}
    \tag{new}\\
basicexpr  & \defined parenexpr \lor charexpr \\
parenexpr  & \defined ( regexpr ) \\
charexpr   & \defined regchar \\
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}

\textbf{Hint:} This is trickier than it seems. 
You may find the following function to be helpful.\footnote{The
function \inlh=pmap= has a similar signature as \inlh=fmap=.
The function \inlh=fmap= is declared as part of the \inlh=Functor= class.
So we can turn our \inlh=Parser= type into an instance of \inlh=Functor= by
using \inlh=pmap= to implement \inlh=fmap=.}

\begin{lstlisting}[numbers=left,commentstyle=\color{OliveGreen}]
-- Parser Map (like map but for parsers not lists)
-- Using a function of type a -> b, it converts
-- a Parser a into a Parser b.
pmap :: (a -> b) -> Parser a -> Parser b
pmap f p = (\inp -> case parse p inp of
                 [] -> []
                 [(v,out)] -> [(f v,out)])
\end{lstlisting}

I ended up using it with the \inlh=concat= function. 
In my implementation 
of \inlh=repeatexpr=, I had an instance of \inlh=Parser [String]= but I
needed an instance of a \inlh=Parser String=.

\item \label{ex:concatexpr} The next operation is concatenation. 
Concatenation doesn't require a special operator. Concatenation is 
written by just writing one regular expression after another (using
parentheses if necessary). Write a
function \inlh=concatexpr :: Parser (Parser String)= that can parse a 
$repeatexpr$ followed by a $concatexpr$ or $\epsilon$. 
For example, we can now parse regular expressions like
``a(b)*a''.  See figure~\ref{fig:concatexpr}.

\begin{figure}
\caption{The grammar for $concatexpr$ for exercise~\ref{ex:concatexpr}.}
\label{fig:concatexpr}
\begin{align*}
regexpr & \defined  \navyblue concatexpr  \tag{changed} \\
\navyblue concatexpr & \navyblue \defined repeatexpr \{concatexpr \lor
    \epsilon \} \tag{new}\\
repeatexpr & \defined basicexpr \{* \lor \epsilon\} \\
basicexpr  & \defined parenexpr \lor charexpr \\
parenexpr  & \defined ( regexpr ) \\
charexpr   & \defined regchar \\
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}

\begin{lstlisting}[numbers=left,commentstyle=\color{OliveGreen}]
> let p = compile "a(b)*a"
> parse p "aa"
[("aa","")]
> parse p "aba"
[("aba", "")]
> parse p "abbbac"
[("abbba", "c")]
\end{lstlisting}

\textbf{Hint:} This one is also tricky. 
You may find the following function useful.
It ``lifts'' a function that works on raw types so that it does
something analogous on \inlh=Parser= types.\footnote{I didn't invent
this function. It is a ``copy'' of \inlh=liftM2= defined in 
\inlh=Control.Monad=. If we defined our \inlh=Parser= to be a Monad
we could have just used \inlh=liftM2= directly.}

\begin{lstlisting}[numbers=left,commentstyle=\color{OliveGreen}]
-- lift the function f up to work on parsers
liftP2 :: (a1 -> a2 -> r) -> Parser a1 -> Parser a2 -> Parser r
liftP2 f p q = p >>= \v1 ->
               q >>= \v2 ->
               return (f v1 v2)
\end{lstlisting}

I used it with the \inlh=++= operator. 
In my implementation
of \inlh=concatexpr=, I had two instances of \inlh=Parser String= but wanted
to concatenate two instances of \inlh=String=.

\item \label{ex:orexpr} Finally, our last operator: `|'. This allows a choice
in pattern matching. The regular expression ``a|b'' matches either ``a'' or
``b''.  Write a function \inlh=orexpr :: Parser (Parser String)= which parses a
$concatexpr$ optionally followed by a `|' and an $orexpr$. 
See the full grammar in figure~\ref{fig:orexpr}.

\begin{lstlisting}[numbers=left,commentstyle=\color{OliveGreen}]
> let p = compile "a|b|c"
> parse p "cd"
[("c","d")]
> parse p "ba"
[("b","a")]
> parse p "aa"
[("a","a")]
\end{lstlisting}

\begin{figure}
\caption{The grammar for $orexpr$ in exercise~\ref{ex:orexpr}.}
\label{fig:orexpr}
\begin{align*}
regexpr & \navyblue \defined orexpr \\
\navyblue orexpr  & \navyblue \defined concatexpr 
    \{\, \mathop{|} orexpr \lor \epsilon \} \\
concatexpr & \defined repeatexpr \{ concatexpr \lor \epsilon \} \\
repeatexpr & \defined basicexpr \{ * \lor \epsilon \} \\
basicexpr  & \defined parenexpr \lor charexpr \\
parenexpr  & \defined ( regexpr ) \\
charexpr   & \defined regchar \\
regchar    & \defined \text{any character except \{`|',`(',`)',`*'\}}
\end{align*}
\end{figure}

\item Finally we want a function 
\inlh=accepts :: Pattern -> String -> Bool= that takes a regular expression
pattern and a string and returns true if the regular expression matches
the string, false otherwise. For it to be a true match the regular
expression must consume the entire string. 
For example the regular expression "a" does not match the string "ab".

\begin{lstlisting}
> accepts "a" "ab"
False
\end{lstlisting}

I defined \inlh=Pattern= like this: 
\begin{lstlisting}
type Pattern = String
\end{lstlisting}

So it is just an alias for String. But it makes the type signature
of \inlh=accepts= easier to understand.

We can now test our library with some examples. Here's a script to test
for phone numbers and continued fractions.


\lstinputlisting[numbers=left,commentstyle=\color{OliveGreen}]{phone.hs}

Now we can check to see if something is a phone number. (Unfortunately,
we can't check for parentheses since we made them special characters. An
enhancement you can make is to allow parentheses by escaping them with
a backslash, and then escaping a backslash with a backslash.") 

\begin{lstlisting}[numbers=left,commentstyle=\color{OliveGreen}]
> isPhoneNum "414.555.0162"
True
> isPhoneNum "414-555-0162"
True
> isPhoneNum "766-4031"
True
> isPhoneNum "414-555.0162"
False
> isCFract "[1]"
True
> isCFract "[1;3]"
True
> isCFract "[1,3]"
False
> isCFract "[1;3,4,5,6]"
True
\end{lstlisting}


\end{enumerate}









\end{document}
