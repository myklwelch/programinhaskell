Programming In Haskell
----------------------
This folder contains materials related to the Programming In Haskell 
study group. You may find examples from the text, answers to exercises,
code related to examples in the book, etc.

Feel free to add your own.

Main Branch
-----------
Main development for this folder should take place in the ProgramInHaskell
branch. I will occasionally merge into the master branch.
