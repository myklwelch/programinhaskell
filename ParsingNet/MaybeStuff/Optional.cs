﻿using System;

namespace MaybeStuff{    public struct Optional<T>     {        private readonly T value;        private readonly bool hasValue;        private Optional(T value)         {            hasValue = true;            this.value = value;        }        public static readonly Optional<T> Nothing = default(Optional<T>);        public static Optional<T> Just(T value)        {            return new Optional<T>(value);        }        public Optional<TResult> Select<TResult>(Func<T, TResult> func)        {            var ourValue = value;            return SelectMany(t => Optional<TResult>.Just(func(ourValue)), (_,y) => y);        }        public Optional<T2> SelectMany<T2>(Func<T, Optional<T2>> func)        {            return SelectMany(func, (x, y) => y);        }        public Optional<TResult> SelectMany<T2, TResult>(Func<T, Optional<T2>> func, Func<T, T2, TResult> func2)        {            var result1 = hasValue ? func(value) : Optional<T2>.Nothing;            return (result1.hasValue) ? Optional<TResult>.Just(func2(value, result1.value)) : Optional<TResult>.Nothing;        }        public Optional<T> Where(Func<T, bool> predicate)        {            return SelectMany(x => predicate(x) ? Just(x) : Nothing);        }

        public override string ToString()
        {
            return hasValue ? "Just " + value : "Nothing";
        }        public static implicit operator Optional<T>(T value)
        {
            return Equals(value, null) ? Nothing : Just(value);
        }        public Optional<TResult> Join<TInner, TKey, TResult>(Optional<TInner> inner, Func<T, TKey> outterKeySelector,
            Func<TInner, TKey> innerKeySelector, Func<T, TInner, TResult> resultSelector)
        {
            if (!hasValue || !inner.hasValue) return Optional<TResult>.Nothing;

            var outterKey = outterKeySelector(value);
            var innerKey = innerKeySelector(inner.value);

            return Equals(innerKey, outterKey)
                       ? resultSelector(value, inner.value)
                       : Optional<TResult>.Nothing;
        }

        public Optional<TResult> GroupJoin<TInner, TKey, TResult>(Optional<TInner> inner, Func<T, TKey> outterKeySelector,
            Func<TInner, TKey> innerKeySelector, Func<T, Optional<TInner>, TResult> resultSelector)
        {
            if (!hasValue || !inner.hasValue) return Optional<TResult>.Nothing;

            var outterKey = outterKeySelector(value);
            var innerKey = innerKeySelector(inner.value);

            return Equals(innerKey, outterKey)
                       ? resultSelector(value, inner)
                       : Optional<TResult>.Nothing;
        }

        public Optional<TResult> Cast<TResult>()
        {
            return hasValue
                       ? Optional<TResult>.Just((TResult)(object)value)
                       : Optional<TResult>.Nothing;
        }

        public bool Contains(T value)
        {
            return hasValue ? Equals(this.value, value) : false;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            var other = obj as Optional<T>?;
            if (other == null) return false;
            return (Equals(value, other.Value.value));

        }

        public override int GetHashCode()
        {
            return (Equals(value, null)) ? 0 : value.GetHashCode();
        }



        public T Value { get { return value; } }    }}