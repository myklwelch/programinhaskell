﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ParsingNet
{



    public static class Parser
    {
        public static Parser<T> Success<T>(T value)
        {
            return (Parser<T>) (input => ParseResult<T>.Success(value, input));

        }

        public static readonly Parser<char> Item =
            (Parser<char>) (input => (string.IsNullOrEmpty(input))
                ? ParseResult<char>.Empty
                : ParseResult<char>.Success(input.Head(), input.Tail()));



        public static readonly Func<Predicate<char>, Parser<char>> Sat = predicate =>
                from item in Item
                where predicate(item)
                select item;



        public static readonly Func<char, Parser<char>> Char = expected => Sat(actual => expected == actual);
        


        public static readonly Parser<char> Lower = Sat(char.IsLower);


        public static readonly Parser<char> Upper = Sat(char.IsUpper);


        public static readonly Parser<char> AlphaNumeric = Sat(char.IsLetterOrDigit);




        public static readonly Parser<char> Letter = Sat(char.IsLetter);

        public static readonly Parser<char> Digit = Sat(char.IsDigit);




        public static readonly Func<string, Parser<IEnumerable<char>>> Chars = s => (string.IsNullOrEmpty(s))
            ? Success(Enumerable.Empty<char>())
            : from c in Char(s.Head())
                from cs in Chars(s.Tail())
                select c.Cons(cs);


        public static readonly Func<string, Parser<string>> String = s =>
                from allChars in Chars(s)
                select new String(allChars.ToArray());

        public static readonly Parser<Nothing> WhiteSpace = from c in (Sat(char.IsWhiteSpace)).Repeat()
                                                            select Nothing.Instance;


        public static readonly Parser<string> Ident = 
                       from c in Lower
                       from chs in AlphaNumeric.Repeat()
                       select new string(c.Cons(chs).ToArray());

        public static readonly Parser<string> Identifier = Ident.Token();


        public static readonly Parser<ulong> Nat =
                       from d in Digit.RepeatOneOrMore()
                       select ulong.Parse(new string(d.ToArray()));

        public static readonly Parser<ulong> Natural = Nat.Token();


        public static readonly Func<string, Parser<string>> Symbol = s => String(s).Token();


        public static readonly Parser<IEnumerable<ulong>> NaturalList = 
                             from lbrace in Symbol("[")
                             from n in Natural
                             from ns in
                                      (from comma in Symbol(",")
                                      from n2 in Natural
                                      select n2).Repeat()
                             from rbrace in Symbol("]")
                             select n.Cons(ns);

    }



    public struct Nothing
    {
        public static readonly Nothing Instance = new Nothing();
    }
}
