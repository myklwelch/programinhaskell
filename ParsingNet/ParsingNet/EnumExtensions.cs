
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ParsingNet
{
    public static class EnumExtensions
    {

        public static IEnumerable<T> ToList<T>(this T element)
        {
            if (element == null) throw new ArgumentNullException("Can't have null element");
            yield return element;
        }

        public static IEnumerable<T> Cons<T>(this T element, IEnumerable<T> list)
        {
            if (element == null) throw new ArgumentNullException("Can't have null element in Cons");
            if (list == null) throw new ArgumentNullException("Can't have null list in Cons");

            return new ConsCell<T>(element, list);
        }

        private abstract class List<T> : IEnumerable<T>
        {
            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public abstract IEnumerator<T> GetEnumerator();
        }

        private class Nil<T> : List<T>
        {
            public override IEnumerator<T> GetEnumerator()
            {
                yield break;
            }
        }

        private class ConsCell<T> : List<T>
        {
            private readonly T head;
            private readonly IEnumerable<T> tail;
            public ConsCell(T head, IEnumerable<T> tail)
            {
                this.head = head;
                this.tail = tail;
            }

            public override IEnumerator<T> GetEnumerator()
            {
                yield return head;
                foreach (T t in tail) yield return t;
            }
        }


    }
}
