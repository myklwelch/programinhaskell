namespace ParsingNet
{

    public static class StringExtensions
    {
        public static char Head(this string str)
        {
            return str[0];
        }

        public static string Tail(this string str)
        {
            return str.Substring(1);
        }


    }
}
