﻿using NUnit.Framework;
using TestClassAttribute=NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute=NUnit.Framework.TestAttribute;

namespace ParsingNet
{
    using P = Parser;

    [TestClass]
    public class ItemTests 
    {
        [TestMethod]
        public void ItemWithEmptyStringFails()
        {
            Assert.AreEqual(P.Item.Parse(""), ParseResult<char>.Empty);
        }

        [TestMethod]
        public void ItemWithOneCharStringSucceeds()
        {
            Assert.AreEqual(P.Item.Parse("a"), ParseResult<char>.Success('a', ""));
        }
    }
}
