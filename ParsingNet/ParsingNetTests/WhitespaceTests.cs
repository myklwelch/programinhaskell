﻿using NUnit.Framework;
using TestClassAttribute=NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute=NUnit.Framework.TestAttribute;

namespace ParsingNet
{
    using P = Parser;
    [TestClass]
    public class WhitespaceTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            P.WhiteSpace.Parse("");
            P.WhiteSpace.Parse("s");
            P.WhiteSpace.Parse(" ");
            P.WhiteSpace.Parse("  ");

            var parser = P.Ident;

            var result = P.Identifier.Parse("hello");

        }
    }
}
