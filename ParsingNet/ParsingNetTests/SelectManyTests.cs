﻿using System;
using NUnit.Framework;
using TestClassAttribute=NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute=NUnit.Framework.TestAttribute;

namespace ParsingNet
{
    using P = Parser;
    [TestClass]
    public class SelectManyTests 
    {
        [TestMethod]
        public void TestMethod1()
        {
            var result = from s in P.String("hello")
                         select s.ToUpper();

            Assert.AreEqual(result.Parse("hello there"), ParseResult<string>.Success("HELLO", " there"));

        }

        [TestMethod]
        public void TestSelectMany3()
        {
            var result = from s in P.String("hi")
                         from s2 in P.Symbol("==")
                         select s + s2;

            Assert.AreEqual(result.Parse("hi   ===="), ParseResult<string>.Success("hi==", "=="));
        }
    }
}
