﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TestClassAttribute=NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute=NUnit.Framework.TestAttribute;

namespace ParsingNet
{
    using P = Parser;
    [TestClass]
    public class RepeatTests 
    {
        [TestMethod]
        public void TestMethod1()
        {
		
            var result = from d in P.Digit.RepeatOneOrMore()
                         select ulong.Parse(new String(d.ToArray()));

            var x = result.Parse("314").Value;
			Assert.AreEqual((uint)314, x);
        }
    }
}
