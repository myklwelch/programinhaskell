﻿// Learn more about F# at http://fsharp.net

module Module1
open System

type 'a Parser = P of (string -> ('a * string) list)

let failure : ('a Parser) = P (fun input -> List.empty)

let success (value : 'a) : ('a Parser) = P (fun input -> [(value, "")])

let item : (char Parser) = P (fun input -> if (String.length(input) = 0) then [] else [(input.Chars(0), input.Substring(1))])

let parse (P parser) input = parser input

let bind (p : 'a Parser, f : ('a -> 'b Parser)) : ('b Parser) = P (fun input -> match parse p input with [(x, remain)] -> parse (f x) remain
                                                                                                       | _ -> [] )
type ParserBuilder() =
    member this.Return(x) : 'a Parser = P (fun input -> [(x,input)])
    member this.ReturnFrom(p: 'a Parser) = p
    member this.Bind(p, rest) = bind(p, rest)

let parser = new ParserBuilder()

(* Turns any existing parser into a unit Parser. In other words it parses and throws away its result *)
let scan p = parser { let! _ = p
                      return () }

let char (c : char) = P (fun input -> match parse item input with [(v,out)] as result when v = c -> result
                                                                | _ -> [])

let sat(pred) : char Parser = parser { let! ch = item
                                       if (pred ch) then return ch else return! failure }


let letter = sat Char.IsLetter

let digit = sat Char.IsDigit

let alphaNum = sat Char.IsLetterOrDigit

let lower = sat Char.IsLower

let upper = sat Char.IsUpper

let isSpace = Char.IsWhiteSpace

let (+++) f g = P (fun input -> match parse f input with [(value, output)] as result -> result
                                                       | _ -> parse g input)

let rec many p = (many1 p) +++ parser { return [] }

and many1 p = parser { let! v = p
                       let! vs = many p
                       return v :: vs }                     

let space = parser { do! scan (many (sat isSpace))
                     return () }
                     
let ident = parser { let! c = lower
                     let! cs = many alphaNum
                     return c :: cs }

let num = parser { let! ds = many1 digit
                   let array = ds |> List.toArray
                   return Int32.Parse(new String(array)) }



let rec charList cs = match cs with
                      | [] -> parser { return [] }
                      | x :: xs -> parser { do! char x |> scan
                                            do! charList xs |> scan
                                            return cs }

let string s = s |> Seq.toList |> charList |> fun p -> parser { let! cs = p
                                                                let csArray = cs |> List.toArray
                                                                return new String(csArray) }

let token p = parser { do! space
                       let! v = p
                       do! space
                       return v }

let number = token num

let identifier = token ident

let symbol (xs : String) = token 
                      
let swap f x y = f y x


